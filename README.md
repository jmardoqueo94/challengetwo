The second challenge was done with nodejs and it will be possible to simulate the operation of the sale of oranges, 3 containers were defined by default and each container will have 5 oranges inside, after making purchases of oranges the containers will be emptied and changing position and the on the right they will be filled with apples and when necessary you can reset the products to start over.

The routes to use from postman are:

GET

- localhost:4000/containers

This route allows you to see the containers and visualize how they are empty of oranges and filled with apples.

POST 

- localhost:4000/buy

This route simulates the purchase of an orange or apple when the oranges are finished.

GET 

- localhost:4000/reset

This route allows you to reset the default values of the containers

Commands needed to run the project:
- npm install
- npm start