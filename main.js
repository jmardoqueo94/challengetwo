const express = require('express');
const app = express();

const products_by_container = 5;

let containers = [
  {
    order: 1,
    value: [],
  },
  {
    order: 2,
    value: [],
  },
  {
    order: 3,
    value: [],
  },
];

const fillProducts = (productName = 'orange') => {
  containers.forEach(({ value }) => {
    value.push(...addProductValues(productName));
  });
};

const addProductValues = (productName) => {
  return [...Array(products_by_container).keys()].map((product) => ({
    productName,
    index: product,
  }));
};

app.get('/containers', (_, res) => res.json(containers));

app.get('/reset', (_, res) => {
  containers = containers.map((_, index) => ({
    order: index + 1,
    value: addProductValues('orange'),
  }));
  res.json({
    data: 'Reset done',
  });
});

app.post('/buy', (_, res) => {
  const container = containers.sort((a, b) => a.order - b.order)?.[0] || null;

  if (container) {
    container.value.shift();

    if (container.value.length === 0) {
      containers.shift();
      containers = [
        ...containers.map((container, index) => ({
          ...container,
          order: index + 1,
        })),
        {
          order: containers.length + 1,
          value: addProductValues('apple'),
        },
      ];
    }
  }

  res.json({
    data: 'Success',
  });
});

app.listen(4000, () => fillProducts());
